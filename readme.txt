http://localhost:8080/retro/api/sprint/all
http://localhost:8080/retro/api/sprint/Sprint5
http://localhost:8080/retro/api/sprint/add
{
    "_id": 5,
    "comments": [
        {
            "bad": "Nothing",
            "good": "Everything was good.",
            "improvement": "Planning throughly",
            "rating": "TWO",
            "starPerformerID": 1,
            "user": "User3",
            "userID": 3
        },
        {
            "bad": "Everything",
            "good": "Everuthing was good.",
            "improvement": "Planning throughly",
            "rating": "TWO",
            "starPerformerID": 1,
            "user": "User3",
            "userID": 3
        },
        {
            "bad": "Something",
            "good": "Everuthing was good.",
            "improvement": "Planning throughly",
            "rating": "THREE",
            "starPerformerID": 1,
            "user": "User3",
            "userID": 3
        }
    ],
    "endDate": "2017-08-08",
    "name": "Sprint5",
    "startDate": "2017-07-26"
}

=>use db retro
=>db.createCollection("Sprints")
=>var sprints= 
[
    {
        "_id": 1,
        "endDate": "2017-07-11",
        "name": "Sprint1",
        "comments": [
            {
                "bad": "Stories were not clear.",
                "good": "Sprint was good.",
                "improvement": "Increase velocity.",
                "rating": "THREE",
                "starPerformerID": 3,
                "user": "User1",
                "userID": 1
            },
            {
                "bad": "Quality.",
                "good": "Good demo.",
                "improvement": "Automative tests.",
                "rating": "THREE",
                "starPerformerID": 3,
                "user": "User2",
                "userID": 2
            },
            {
                "bad": "Collaboration.",
                "good": "Stories were completed.",
                "improvement": "Can take up more stories",
                "rating": "FOUR",
                "starPerformerID": 1,
                "user": "User3",
                "userID": 3
            }
        ],
        "startDate": "2017-06-28"
    },
    {
        "_id": 2,
        "endDate": "2017-07-25",
        "name": "Sprint2",
        "comments": [
            {
                "bad": "Too many holidays.",
                "good": null,
                "improvement": "Effeciency.",
                "rating": "THREE",
                "starPerformerID": 2,
                "user": "User1",
                "userID": 1
            },
            {
                "bad": null,
                "good": "Good team work.",
                "improvement": null,
                "rating": "THREE",
                "starPerformerID": 1,
                "user": "User2",
                "userID": 2
            },
            {
                "bad": "Nothing",
                "good": "Everuthing was good.",
                "improvement": "Planning throughly",
                "rating": "THREE",
                "starPerformerID": 1,
                "user": "User3",
                "userID": 3
            }
        ],
        "startDate": "2017-07-12"
    }
];
=>db.Sprints.insert(sprints);
=>db.Sprints.find().forEach(printjson)