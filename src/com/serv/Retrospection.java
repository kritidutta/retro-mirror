/*
 *  $URL$
 *  $Date$
 *  
 *  $Copyright-Start$
 *
 *  Copyright (c) 2017
 *  KSD Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by KSD Corporation.
 *
 *  KSD Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by KSD Corporation.
 *
 *  $Copyright-End$
 */

package com.serv;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.base.Sprint;
import com.db.MongoInterface;
import com.db.PersitantInterface;

@Path("/sprint")
public class Retrospection  {

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Sprint> getSprints() {
        
        PersitantInterface persitantInterface = new MongoInterface();  
        return persitantInterface.getSprints();
    }

    @GET
    @Path("/{sprint}")
    @Produces(MediaType.APPLICATION_JSON)
    public Sprint getSprint( @PathParam("sprint") String sprintName) {

        PersitantInterface persitantInterface = new MongoInterface();  
        return persitantInterface.getSprint(sprintName);
    }

    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPerson(Sprint newSprint) {

        PersitantInterface persitantInterface = new MongoInterface();
        persitantInterface.addSprint(newSprint);

        return Response.status(200).build();
    }

}
