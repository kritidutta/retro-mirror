/*
 *  $URL$
 *  $Date$
 *  
 *  $Copyright-Start$
 *
 *  Copyright (c) 2017
 *  KSD Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by KSD Corporation.
 *
 *  KSD Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by KSD Corporation.
 *
 *  $Copyright-End$
 */

package com.db;

import com.base.Sprint;

import java.util.List;

public interface PersitantInterface {

    public List<Sprint> getSprints();

    public Sprint getSprint(String name);

    public void addSprint(Sprint sprint);

}
