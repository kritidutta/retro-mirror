/*
 *  $URL$
 *  $Date$
 *  
 *  $Copyright-Start$
 *
 *  Copyright (c) 2017
 *  KSD Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by KSD Corporation.
 *
 *  KSD Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by KSD Corporation.
 *
 *  $Copyright-End$
 */

package com.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.base.Comment;
import com.base.Ratings;
import com.base.Sprint;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoInterface implements PersitantInterface {

    private static final String DB_HOST = "localhost";
    private static final int DB_PORT = 27017;
    private static final String DB_NAME = "retrospect";
    private static final String SPRINT_COLLECTION = "Sprints";

    @Override
    public List<Sprint> getSprints() {

        List<Sprint> sprintList = new ArrayList<Sprint>();
        MongoClient mongoClient = null;

        try{            
            mongoClient = new MongoClient(DB_HOST, DB_PORT);
            MongoDatabase db = mongoClient.getDatabase(DB_NAME);           
            MongoCollection<Document> coll = db.getCollection(SPRINT_COLLECTION);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

            FindIterable<Document> docs = coll.find();
            if (docs != null) {  

                for (Document current : docs) {
                    Sprint sprint = new Sprint();

                    sprint.set_id(current.getDouble("_id"));
                    sprint.setName(current.getString("name"));

                    String endDateStr = current.getString("endDate");
                    sprint.setendDate(sdf.parse(endDateStr));

                    String startDateStr = current.getString("startDate");
                    sprint.setstartDate(sdf.parse(startDateStr));

                    List<Comment> sprintComments = null; 
                    if(current.containsKey("comments")) {

                        List<Document> currentComments = (List<Document>)current.get("comments");                    
                        if(!currentComments.isEmpty()) {

                            sprintComments = new ArrayList<Comment>();

                            for (Document currentComment : currentComments) {

                                Comment sprintComment = new Comment();

                                sprintComment.setUserID(currentComment.getDouble("userID"));
                                sprintComment.setStarPerformerID(currentComment.getDouble("starPerformerID"));

                                sprintComment.setUser(currentComment.getString("user"));
                                sprintComment.setGood(currentComment.getString("good"));
                                sprintComment.setBad(currentComment.getString("bad"));
                                sprintComment.setImprovement(currentComment.getString("improvement"));

                                sprintComment.setRating(Ratings.valueOf(currentComment.getString("rating")));

                                sprintComments.add(sprintComment);
                            }
                        }
                    }

                    if(sprintComments != null)
                        sprint.setComments(sprintComments);

                    sprintList.add(sprint);
                }
            }
        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        } finally {
            if(mongoClient != null) {
                mongoClient.close();
            }         
        }    

        return sprintList;
    }

    @Override
    public Sprint getSprint(String name) {

        MongoClient mongoClient = null;
        Sprint sprint = null;

        try{              
            mongoClient = new MongoClient(DB_HOST, DB_PORT);
            MongoDatabase db = mongoClient.getDatabase(DB_NAME);           
            MongoCollection<Document> coll = db.getCollection(SPRINT_COLLECTION);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

            BasicDBObject whereQuery = new BasicDBObject();
            whereQuery.put("name", name);

            FindIterable<Document> docs = coll.find(whereQuery);

            if(docs != null) {    

                Document current = docs.first();
                sprint = new Sprint();

                sprint.set_id(current.getDouble("_id"));
                sprint.setName(current.getString("name"));

                String endDateStr = current.getString("endDate");
                sprint.setendDate(sdf.parse(endDateStr));

                String startDateStr = current.getString("startDate");
                sprint.setstartDate(sdf.parse(startDateStr));

                List<Comment> sprintComments = null; 
                if(current.containsKey("comments")) {
                    
                    List<Document> currentComments = (List<Document>)current.get("comments");                    
                    if(!currentComments.isEmpty()) {

                        sprintComments = new ArrayList<Comment>();

                        for (Document currentComment : currentComments) {

                            Comment sprintComment = new Comment();

                            sprintComment.setUserID(currentComment.getDouble("userID"));
                            sprintComment.setStarPerformerID(currentComment.getDouble("starPerformerID"));

                            sprintComment.setUser(currentComment.getString("user"));
                            sprintComment.setGood(currentComment.getString("good"));
                            sprintComment.setBad(currentComment.getString("bad"));
                            sprintComment.setImprovement(currentComment.getString("improvement"));

                            sprintComment.setRating(Ratings.valueOf(currentComment.getString("rating")));

                            sprintComments.add(sprintComment);
                        }
                    }

                    if(sprintComments != null)
                        sprint.setComments(sprintComments);
                }
            }
        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        } finally {
            if(mongoClient != null) {
                mongoClient.close();
            }         
        }    

        return sprint;
    }

    @Override
    public void addSprint(Sprint sprint) {

        MongoClient mongoClient = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

        try{            
            mongoClient = new MongoClient(DB_HOST, DB_PORT);
            MongoDatabase db = mongoClient.getDatabase(DB_NAME);           
            MongoCollection<Document> coll = db.getCollection(SPRINT_COLLECTION);

            List<Document> docComments = null;
            List<Comment> sprintComments = sprint.getComments();
            
            if(!sprintComments.isEmpty()) {
                docComments = new ArrayList<Document>();
                
                for(Comment sprintComment:sprintComments) {
                    Document doc = new Document();
                    
                    doc.append("userID", sprintComment.getUserID()).
                    append("user;", sprintComment.getUser()).
                    append("good", sprintComment.getGood()).
                    append("bad", sprintComment.getBad()).
                    append("improvement", sprintComment.getImprovement()).
                    append("starPerformerID", sprintComment.getStarPerformerID()).
                    append("rating", sprintComment.getRating().toString());
                    
                    docComments.add(doc);
                }
            }
            
            Document doc = null;
            if(docComments == null) {
                doc = new Document(
                    "_id", sprint.get_id()).
                        append("name", sprint.getName()).
                        append("endDate", sdf.format(sprint.getendDate())).
                        append("startDate", sdf.format(sprint.getstartDate()));

                coll.insertOne(doc);
            } else {
                doc = new Document(
                    "_id", sprint.get_id()).
                        append("name", sprint.getName()).
                        append("endDate", sdf.format(sprint.getendDate())).
                        append("startDate", sdf.format(sprint.getstartDate())).
                        append("comments", docComments);

                coll.insertOne(doc);
            }

        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        } finally {
            if(mongoClient != null) {
                mongoClient.close();
            }         
        }    
    }

}
