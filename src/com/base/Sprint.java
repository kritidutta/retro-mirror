/*
 *  $URL$
 *  $Date$
 *  
 *  $Copyright-Start$
 *
 *  Copyright (c) 2017
 *  KSD Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by KSD Corporation.
 *
 *  KSD Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by KSD Corporation.
 *
 *  $Copyright-End$
 */

package com.base;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.owlike.genson.annotation.JsonDateFormat;

@XmlRootElement
public class Sprint {
    
    public double get_id() {
        return _id;
    }
    
    public void set_id(double id) {
        _id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setstartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public Date getstartDate() {
        return startDate;
    }
    
    public void setendDate(Date endDate) {
        this.endDate = endDate;
    }
    
    public Date getendDate() {
        return endDate;
    }
        
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
    
    public List<Comment> getComments() {
        return comments;
    }
    
    public Sprint() {
        super();
    }
    
    double _id;
    String name;
    @JsonDateFormat("yyyy-mm-dd") 
    Date startDate;
    @JsonDateFormat("yyyy-mm-dd") 
    Date endDate;
    List<Comment> comments;
    
}
