/*
 *  $URL$
 *  $Date$
 *  
 *  $Copyright-Start$
 *
 *  Copyright (c) 2017
 *  KSD Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by KSD Corporation.
 *
 *  KSD Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by KSD Corporation.
 *
 *  $Copyright-End$
 */

package com.base;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Comment {
    
    public double getUserID() {
        return userID;
    }
    
    public String getUser() {
        return user;
    }
    
    public String getGood() {
        return good;
    }
    
    public String getBad() {
        return bad;
    }
    
    public String getImprovement() {
        return improvement;
    }
    
    public double getStarPerformerID() {
        return starPerformerID;
    }
    
    public Ratings getRating() {
        return rating;
    }
    
    public void setUserID(double userID) {
        this.userID = userID;
    }
    
    public void setUser(String user) {
        this.user = user;
    }
    
    public void setGood(String good) {
        this.good = good;
    }
    
    public void setBad(String bad) {
        this.bad = bad;
    }
    
    public void setImprovement(String improvement) {
        this.improvement = improvement;
    }
    
    public void setStarPerformerID(double jid) {
        starPerformerID = jid;
    }
    
    public void setRating(Ratings rating) {
        this.rating = rating;
    }
    
    double userID;
    String user;
    String good;
    String bad;
    String improvement;
    double starPerformerID;
    Ratings rating;
    
}
