/*
 *  $URL$
 *  $Date$
 *  
 *  $Copyright-t$
 *
 *  Copyright (c) 2017
 *  KSD Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by KSD Corporation.
 *
 *  KSD Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by KSD Corporation.
 *
 *  $Copyright-End$
 */

package com.base;

public enum Ratings {
    
    ONE, TWO, THREE, FOUR, FIVE
}
